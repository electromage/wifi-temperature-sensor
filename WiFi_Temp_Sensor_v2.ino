/*********
An ESP8266 RESTful temperature sensor for Sparkfun ESP8266 Thing Dev
*********/

#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>

// WiFi Settings
const char* ssid = "<SSID>";
const char* password = "<PASSWORD>";

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

String tempC;
String tempF;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

ESP8266WebServer server(80);

// Serving Content
void getHelloWorld() {
    DynamicJsonDocument doc(512);
    doc["name"] = "Hello world";

    Serial.print(F("Stream..."));
    String buf;
    serializeJson(doc, buf);
    server.send(200, "application/json", buf);
    Serial.print(F("done."));
}

void getTempC() {
    sensors.requestTemperatures();
    tempC = String(sensors.getTempCByIndex(0), 2);
    String jsonStart = "{\"temperature_C\" : \"";
    String jsonValue = tempC;
    String jsonEnd = "\"}";
    String jsonResponse = jsonStart + jsonValue + jsonEnd;
    server.send(200, "text/json", jsonResponse);
}

void getTemp() {
    sensors.requestTemperatures();
    tempC = String(sensors.getTempCByIndex(0), 2);
    tempF = String(sensors.getTempFByIndex(0), 2);
    
    DynamicJsonDocument doc(512);

    doc ["location"] = "<Sensor Location>";
    doc ["temperature_C"] = tempC;
    doc ["temperature_F"] = tempF;
    
    String buf;
    
    serializeJson(doc, buf);
    server.send(  200, F("application/json"), buf);
}

void getSettings() {
    DynamicJsonDocument doc(512);
 
    doc["ip"] = WiFi.localIP().toString();
    doc["gw"] = WiFi.gatewayIP().toString();
    doc["nm"] = WiFi.subnetMask().toString();

    if (server.arg("signalStrength")== "true"){
        doc["signalStrengh"] = WiFi.RSSI();
    }

    if (server.arg("chipInfo")== "true"){
        doc["chipId"] = ESP.getChipId();
        doc["flashChipId"] = ESP.getFlashChipId();
        doc["flashChipSize"] = ESP.getFlashChipSize();
        doc["flashChipRealSize"] = ESP.getFlashChipRealSize();
    }
    if (server.arg("freeHeap")== "true"){
        doc["freeHeap"] = ESP.getFreeHeap();
    }

    Serial.print(F("Stream..."));
    String buf;
    serializeJson(doc, buf);
    server.send(200, F("application/json"), buf);
    Serial.print(F("done."));
}

// Define routing
void restServerRouting() {
    server.on("/", HTTP_GET, []() {
        server.send(200, F("text/html"),
            F("RESTful temperature server. GET /temperature for the current temperature, /system for system information"));
    });
    server.on(F("/helloWorld"), HTTP_GET, getHelloWorld);
    server.on(F("/system"), HTTP_GET, getSettings);
    server.on(F("/temperature"), HTTP_GET, getTemp);
}
 
// Manage not found URL
void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  Serial.begin(115200);
  sensors.begin();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
 
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
 
  // Activate mDNS this is used to be able to connect to the server
  // with local DNS hostmane esp8266.local
  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
 
  // Set server routing
  restServerRouting();
  // Set not found response
  server.onNotFound(handleNotFound);
  // Start server
  server.begin();
  Serial.println("HTTP server started");
}
 
void loop(void) {
  server.handleClient();
}
